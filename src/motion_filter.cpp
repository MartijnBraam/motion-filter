#include <cstdio>
#include <ctime>
#include <opencv2/opencv.hpp>
using namespace cv;

double getPSNR(const Mat& I1, const Mat& I2)
{
    Mat s1;
    absdiff(I1, I2, s1);       // |I1 - I2|
    s1.convertTo(s1, CV_32F);  // cannot make a square on 8 bits
    s1 = s1.mul(s1);           // |I1 - I2|^2
    Scalar s = sum(s1);        // sum elements per channel
    double sse = s.val[0] + s.val[1] + s.val[2]; // sum channels
    if( sse <= 1e-10) // for small values return zero
        return 0;
    else
    {
        double mse  = sse / (double)(I1.channels() * I1.total());
        double psnr = 10.0 * log10((255 * 255) / mse);
        return psnr;
    }
}

int main(int argc, char** argv )
{
    if ( argc != 4 )
    {
        printf("usage: motion <Video file> <mask file> <threshold>\n\n");
        printf("Video file  : The input video to check\n");
        printf("Mask file   : Image to mask out the video before detection\n");
        printf("              Black means no detection and white means detection\n");
        printf("Threshold   : The PSNR threshold, lower means less sensitive, 20 is a good start\n");
        return -1;
    }

    double threshold = atof(argv[3]);

    VideoCapture video(argv[1]);
    Mat mask;
    mask = imread( argv[2], 1 );

    if (!video.isOpened())
    {
        printf("Could not open video file\n");
        return -1;
    }

    if ( !mask.data )
    {
        printf("No image data in mask file\n");
        return -1;
    }

    Size videoSize = Size((int) video.get(CAP_PROP_FRAME_WIDTH), (int) video.get(CAP_PROP_FRAME_HEIGHT));
    printf("Video resolution is %dx%d\n", videoSize.width, videoSize.height);

    if(video.get(CAP_PROP_FRAME_WIDTH) != mask.cols || video.get(CAP_PROP_FRAME_HEIGHT) != mask.rows){
        printf("Mask resolution is %dx%d\n", mask.cols, mask.rows);
        printf("Mask file resolution doesn't match video frame size\n");
        return -1;
    }

    int framenum = -1;
    double psnrV;
    Mat frameReference, frameUnderTest;
    video >> frameReference;
    frameReference = frameReference.mul(mask);

    std::clock_t start;
    start = std::clock();

    for(;;){
        video >> frameUnderTest;
        if(frameUnderTest.empty()){
            printf("DONE!\n");
            double sec = (std::clock() - start) / CLOCKS_PER_SEC;
            printf("Elapsed time: %f sec\n",  sec);
            double fps = framenum / sec;
            printf("Frames per second: %f\n", fps);
            return 0;
        }
        ++framenum;
        frameUnderTest = frameUnderTest.mul(mask);
        psnrV = getPSNR(frameReference,frameUnderTest);
        if(psnrV < threshold){
            printf("Motion detected, starting at frame %d\n", framenum);
            return 1;
        }
        printf("Frame: %d, psnr: %f\n", framenum, psnrV);
    }
}

